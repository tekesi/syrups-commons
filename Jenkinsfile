pipeline {
  agent any
  tools {
    maven 'maven3'
    jdk 'jdk8'
  }
  options {
    timestamps()
    timeout(time: 5, unit: 'MINUTES')
  }
  stages {
    stage('Switch Branch') {
      steps {
        sh '''
          git checkout master
          '''
      }
    }

    stage('Code Analysis') {
      steps {
        withSonarQubeEnv('SonarCloud') {
          sh '''
            mvn sonar:sonar \\
            -Dsonar.projectKey=aka.tekesi:syrups-commons \\
            -Dsonar.organization=tekesi \\
            -Dsonar.host.url=https://sonarcloud.io
            '''
        }
      }
    }
    stage("Quality Gate") {
      steps {
        timeout(time: 5, unit: 'MINUTES') {
          waitForQualityGate abortPipeline: true
        }
      }
    }
    stage('Package') {
      steps {
        sh 'mvn package'
      }
    }
    stage('Deploy') {
      steps {
        sh 'mvn clean deploy -DskipTests'
      }
    }

    stage('Release') {
      input {
        message "Release this version?"
        ok "OK"
        submitter "nxzh"
        parameters { booleanParam(name: 'RELEASE_FLAG', defaultValue: true, description: '') }
      }
      steps {
        script {
          if (RELEASE_FLAG) {
            sh 'mvn release:clean release:prepare'
            sh 'mvn release:perform'
          }
        }
      }
    }
  }
  post {
    always {
      deleteDir()
    }
  }
}